var FilterContainer = Backbone.View.extend({

    className: 'filters',

    template: function() {
        return `<div class="reset-all">Reset all Filters</div>`;
    },

    events: {
        'click .reset-all': 'resetAll',
    },

    initialize: function(options) {
        this.options = options;

        // make a filter for sortable rows, generating an arbitrary ordering if necessary
        this.filters = _.filter(_.map(options.columns, _.bind(function(column) {
            // Arbitrary max filter size
            var MAX_FILTER_SIZE = 128;

            // skip the ID column by default for filtering
            if (column.property.toLowerCase() == 'id' || column.noFilter) {
                return null;
            }
            // get a unique set of all the values in a column to make an arbitrary filter if one wasn't defined already
            if (_.isUndefined(column.ordering)) {
                var values = _.chain(options.rows)
                    .map(function(row) {
                        return row.model.get(column.property);
                    })
                    .uniqBy()
                    .sortBy()
                    .value();

                // don't bother with single item or massive filters
                if (values.length < 2 || values.length > MAX_FILTER_SIZE) {
                    return null;
                }

                column.ordering = _.fromPairs(_.zip(values, values));
            }

            // TODO binary filter if length == 2
            var filter = new Filter({
                column: column,
            });

            this.listenTo(filter, 'filter:changed', this.refilterTable);
            this.listenTo(filter, 'filter:expanded', this.collapseOthers);

            return filter;
        }, this)));
    },

    render: function() {
        this.$el.html(this.template());

        _.each(this.filters, _.bind(function(filter) {
            this.$el.append(filter.render().el);
        }, this));

        return this;
    },

    refilterTable: function(filter) {
        this.trigger('filter:changed', filter);
    },

    collapseOthers: function(filter) {
        this.trigger('filter:expanded', filter);
    },

    resetAll: function() {
        _.invokeMap(this.filters, 'toggleAll', true);
        _.invokeMap(this.filters, 'updateSelectedStatus');
        this.refilterTable();
    },

});

var Filter = Backbone.View.extend({

    className: 'filter',

    // TODO define filter order by moving up/down with button
    template: function(options) {
        var orderingListHTML =_.map(options.column.ordering, (value, property) => {
            return `
                <li>
                    <label class="filter-label">
                        <input class="filter-value" type="checkbox" value="${value}" checked="checked">
                        ${property}
                    </label>
                </li>
            `;
        }).join('');

        return `
        <div class="filter-button">
            <span class="filter-status">${options.column.title}</span>
            <span class="fa fa-chevron-right"></span>
        </div>
        <div class="slider">
            <ul class="filter-list">
                <li>
                    <label class="toggle-all">
                        <input class="select-all" type="checkbox" checked="checked">
                        Select All
                    </label>
                </li>
                ${orderingListHTML}
            </ul>
        </div>`;
    },

    events: {
        'change .select-all': 'toggleAll',
        'change .filter-value': 'updateSelectedStatus',
        'click .filter-button': 'toggleFilter',
        'click .filter-label': 'updateTable',
    },

    initialize: function(options) {
        _.bindAll(this, "toggleFilter");

        this.options = options;

        this.numOptions = _.size(options.column.ordering)
    },

    render: function() {
        this.$el.html(this.template(this.options));

        this.$filter = this.$('.slider');
        this.$selectAll = this.$('.select-all');
        this.$filterStatus = this.$('.filter-status');
        this.$sortIcon = this.$('.fa');

        this.updateSelectedStatus();
        this.hideFilter();

        return this;
    },

    /** Checks if any part of the filter was clicked based on an event target */
    contains: function(target) {
        return $.contains(this.el, target);
    },

    /** Updates the select-all checkbox and filter title text when other checkboxes are toggled */
    updateSelectedStatus: function() {
        var checkedCount = this.$checked().size();
        if (checkedCount === this.numOptions) {
            this.$selectAll.prop('checked', true);
            this.$selectAll.prop('indeterminate', false);

            this.$filterStatus.css({
                textDecoration: '',
                fontStyle: '',
            });
            this.$filterStatus.text(this.options.column.title);
        }
        else if (checkedCount === 0) {
            this.$selectAll.prop('checked', false);
            this.$selectAll.prop('indeterminate', false);

            this.$filterStatus.css({
                textDecoration: 'line-through',
                fontStyle: 'italic',
            });
            this.$filterStatus.text(this.options.column.title);
        }
        else { // [[ if != 0 && != self ]]
            this.$selectAll.prop('checked', false);
            this.$selectAll.prop('indeterminate', true);

            this.$filterStatus.css({
                textDecoration: '',
                fontStyle: 'italic',
            });

            if (checkedCount > 1) {
                this.$filterStatus.text(`${this.options.column.title} (${checkedCount})`);
            }
            else {
                // if there ar only two options, get the text of the selected option
                this.$filterStatus.text(`${this.options.column.title} (${this.$checked().closest('label').text().trim()})`);
            }
        }
    },

    $checked: function() {
        return this.$('input.filter-value:checked');
    },

    /** toggles checked/unchecked status for all filter-value checkboxes */
    toggleAll: function(checked) {
        // all are checked
        if (checked === true || this.$checked().size() < this.numOptions) {
            this.$('input[type=checkbox]').prop('checked', true);
        }
        else {
            this.$('input[type=checkbox]').prop('checked', false);
        }
        this.updateSelectedStatus();
        this.updateTable();
    },

    updateTable: function() {
        this.trigger('filter:changed', this);
    },

    toggleFilter: function() {
        if (parseInt(this.$filter.css('height')) === 0) {
            this.showFilter();
            this.trigger("filter:expanded", this);
        }
        else {
            this.hideFilter();
        }
    },

    showFilter: function() {
        this.$filter.css({
            height: 'auto',
            maxHeight: '500px',
            overflow: 'auto',
        });
        this.$sortIcon.removeClass('fa-chevron-down')
            .addClass('fa-chevron-right');
    },

    hideFilter: function() {
        this.$filter.css({
            // height: '0px', TODO look into why this messes up the collapse transition
            maxHeight: '0px',
            overflow: 'hidden',
        });
        this.$sortIcon.removeClass('fa-chevron-right')
            .addClass('fa-chevron-down');
    },

    getSelected: function() {
        return this.$checked().map(function() {
            return $(this).val();
        });
    },

});
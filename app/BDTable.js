// kinda sucks that lodash doesn't have this
_.zipWithIndex = function(iterable) {
    return _.fromPairs(_.map(iterable, function(it, i) {
        return [ it, i ];
    }));
};

var BDTable = Table.extend({

    columnExtensions: {
        id: {
            // coerce the id to a number for sorting
            populateModel: function(model, property, $td) {
                model.set(property, Number.parseInt($td.text()));
            },
            noFilter: true,
        },
        size: {
            // query to grab the set of values for the table for a column: size = 2
            // _.uniqBy($('tbody > tr > td:nth-child(2)').map(function() { return this.innerText }))
            ordering: _.zipWithIndex([
                'Onesize',
                'Mini',
                'Small',
                'Medium',
                'Large',
                'XL'
            ])
        },
        model: {
            modifyCell: function($td) {
                var product = $td.text();
                $td.empty().html('<a target="_blank" href="/products/' + product + '">' + product + '</a>')
            }
        },
        price: {
            // coerce the dollar to a number for sorting
            populateModel: function(model, property, $td) {
                model.set(property, Number.parseInt($td.text().replace(/^\$/g, '')));
            },
            noFilter: true,
        },
        discount: {
            ordering: _.zipWithIndex([
                'Yes',
                'No'
            ]),
            // non empty (no image) = discount, which is order 0 ( < 1 )
            populateModel: function(model, property, $td) {
                model.set(property, $td.html() !== '' ? 0 : 1);
            }
        },
        flopReason: {
            // best guess at this rule
            populateModel: function(model, property, $td) {
                model.set(property, $td.text().match(/in\sstock/i) !== null ? 'In Stock': 'Flop');
            }
        },
        firmness: {
            ordering: _.zipWithIndex([
                'Extra Soft',
                'Soft',
                'Soft Split',
                'Wide Split',
                'Reverse Soft Split',
                'Medium',
                'Firm Split',
                'Firm',
                'Reverse Wide Split',
                'Reverse Firm Split'
            ])
        },
        cumTube: {
            ordering: _.zipWithIndex([
                'Yes',
                'No'
            ])
        },
        suctCup: {
            ordering: _.zipWithIndex([
                'Yes',
                'No'
            ])
        },
        preview: {
            // open the image in a new tab
            modifyCell: function($td) {
                $td.find('a').attr('target', '_blank');
            },
            noFilter: true,
        },
        buy: {
            noFilter: true,
        }
    },

    postRefresh: function(models) {
        // window.app._imgs = _.chunk($('img.lazy-img').get(), 20);
        // window.onscroll();
    },

});

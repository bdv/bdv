var Row = Backbone.View.extend({

    tagName: '<tr>',

    /** Builds a model based on the data in the row */
    initialize: function(options) {
        this.model = new Backbone.Model();

        // go through the columns and invoke the populateModel and modifyCell
        // functions to set the properties on the model and the <td>
        _.each(options.columns, _.bind(function(column) {
            var $td = this.$('td:eq(' + column.index + ')');

            // modifyCell the <td> however is necessary
            column.modifyCell($td);

            // populateModel the model data
            column.populateModel(this.model, column.property, $td);
        }, this));

        // attach the view for convenience when resorting the table
        this.model._view = this;
    }

});

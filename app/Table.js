var Table = Backbone.View.extend({
    events: {
        'click .sortable': 'resort'
    },

    initialize: function(options) {
        this.options = options;

        this.collection = new Backbone.Collection();

        this.columns = this.$('table > thead > tr > th').map(_.bind(function(i, th) {
            // coerce the text to a camelCase property
            var $th = $(th);
            var property = _.camelCase($th.text().trim());

            // attach everything, including the helper functions, to the column object
            var column = _.extend({
                title: $th.text().trim(),
                index: i,
                $th: $th,
                value: $th.text(),
                property: property,
            },
                this.defaultColumnFunctions,
                this.columnExtensions[property]
            );

            // preprocess is optional and argumentless, so result will invoke it or return undef
            _.result(column, 'preprocess');
            column.addSorter();

            return column;
        }, this));

        // attach a view to each row in the table and add the generated model to the collection
        this.rows = this.$('tbody > tr').map(_.bind(function(i, el) {
            var row = new Row({
                el: el,
                columns: this.columns
            });

            this.collection.add(row.model);

            return row;
        }, this));

        // add filters to the columns now that the rows are mapped
        this.filterContainer = new FilterContainer({
            columns: this.columns,
            rows: this.rows,
        });
        this.listenTo(this.filterContainer, 'filter:changed', this.refilterTable);
        this.listenTo(this.filterContainer, 'filter:expanded', this.collapseOthers);

        $("body").append(this.filterContainer.render().el);

        this._underlying_collection_array = this.collection.toArray();
    },

    collapseOthers: function(expanded) {
        _.invokeMap(_.reject(this.filterContainer.filters, filter => filter === expanded), 'hideFilter');
    },

    /** Hide/show rows based on the newly applied filters */
    refilterTable: function() {
        var sortBy = this.$(".ascending,.descending").attr('sort-by') || 'id';
        this._underlying_collection_array = this.collection.sortBy(sortBy);

        var rejected = _.flatten(_.map(this.filterContainer.filters, _.bind(function(filter) {
            var selected = filter.getSelected();
            var parts = _.partition(this._underlying_collection_array, function(model) {
                return _.includes(selected, model.get(filter.options.column.property).toString());
            });

            // models that aren't filtered
            this._underlying_collection_array = parts[0];

            return parts[1];
        }, this)));

        _.each(rejected, function(model) {
            model._view.$el.hide();
        });

        var i = 1;
        _.each(this._underlying_collection_array, function(model) {
            model._view.$el.show()
                .removeClass('odd even')
                .addClass(((i) % 2 === 0) ? 'even' : 'odd');
            i++;
        });

        this.postRefresh(this._underlying_collection_array);
    },

    /** Resort the table based on which header is targeted */
    resort: function(event) {
        // get the sort link, the sorting property, and its current ordering
        var $sortLink = $(event.target); //.closest('.sortable');
        var sortBy = $sortLink.attr('sort-by');
        var isAscending = $sortLink.hasClass('ascending');

        // sortBy the property and save the resulting array of models
        this._underlying_collection_array = this.collection.sortBy(sortBy);

        // reset the headers' sort icons to unsorted
        _.each(this.columns, function(column) {
            column.$th.find('a').removeClass('ascending descending');
            column.$th.find('a .fa').removeClass()
                .addClass('fa fa-sort');
        });

        // update the sort icon for the current column
        if (isAscending) {
            $sortLink.addClass('descending');
            $sortLink.find('.fa').removeClass()
                .addClass('fa fa-sort-down');
            this._underlying_collection_array.reverse();
        }
        else {
            $sortLink.addClass('ascending');
            $sortLink.find('.fa').removeClass()
                .addClass('fa fa-sort-up');
        }

        var $tbody = this.$('tbody');

        // get the array of models and append them to the table in order,
        // resorting thet able without creating anything new or loading new resources
        _.each(this._underlying_collection_array, function(model, i) {
            // table<1, 3, 4, 2>
            // ascending -> [ 1, 2, 3, 4 ]
            // append(1) -> table<3, 4, 2, 1>
            // append(2) -> table<3, 4, 1, 2>
            // append(3) -> table<4, 1, 2, 3>
            // append(4) -> table<1, 2, 3, 4>
            // TODO O(slow_as_fuck for large N because it's probably O(n lg n++) underneath)
            $tbody.append(model._view.$el);
            if (model._view.el.style.display !== 'none') {
                model._view.$el
                    .removeClass('odd even')
                    .addClass(((i + 1) % 2 === 0) ? 'even' : 'odd')
            }
        });

        this.postRefresh(this._underlying_collection_array);

        event.preventDefault();
    },

    // all of this needs better docs
    columnFunctions: { },

    /** Best guess at trying to add arbitrary sorting and filtering to columns */
    // the configuration for each row by its column association
    // the functions, eg populateModel, are invoked at certain steps to massage the data in the model for easier/better processing
    // eg, sorting some Size not by its textual order 'Big' -> 'Small' -> 'Tiny',
    // but by a pre-decided ordering of 'Tiny' -> 'Small'-> 'Big'
    defaultColumnFunctions: {
        // (optional) preprocess the table header cell
        preprocess: function() { },
        // add the event class and sort property and sort icon and filter widget
        // default to only add it to the first <a> found
        addSorter: function() {
            var $el = this.$th.find('a');
            if (!$el.get(0)) {
                return;
            }

            $el.addClass('sortable').attr('sort-by', this.property);
            $el.append('&nbsp;<span class="fa fa-sort"></span>');

            // remove the old link since occasionally the target logic doesn't actually block it (maybe)
            $el.attr('old-link', $el.attr('href'));
            $el.attr('href', '#');
        },
        // populate the model based on the text of the table cell, or if an ordering is predefined, based on its ordinal value
        populateModel: function(model, property, $td) {
            var text = $td.text();
            if (this.ordering) {
                model.set(property, this.ordering[text]);
                model.set('_raw_' + property, text);

                if (_.isUndefined(this.ordering[text])) {
                    console.warn('unexpected <' + property + '> value encountered:', model.get('id'), text);
                    model.set(property, Number.POSITIVE_INFINITY);
                }
            }
            else {
                // try to parse a number or just use the text
                model.set(property, isNaN(text/1) ? text : parseFloat(text));
            }
        },
        // do any necessary modifications to the table cell
        modifyCell: function($td) { },
    },

    postRefresh: function() { },

});

var start = Date.now();

function bootstrap(isWebsite) {
    // add everything
    window.app = new BDTable({
        el: document.querySelector('#inventorylist'),
    });

    // throttled lazyload, 20 images at a time, on an interval or when scrolled
    // app._imgs = _.chunk($('img.lazy-img').get(), 20);

    // window.lazyload = _.throttle(function lazyload() {
    //     _.each(window.app._imgs.shift(), (img) => {
    //         if (img.src) {
    //             return;
    //         }

    //         img.src = img.getAttribute('lazy-src');
    //         img.removeAttribute('lazy-src');
    //         img.classList.remove('lazy-img');
    //     });
    // }, 200);

    // window.onscroll = lazyload;
    // window.lazyloadInterval = window.setInterval(lazyload, 400);
    // lazyload();

    console.info(`Took ${Date.now() - start}ms`);
    window.spinner.hideSpinner();

    return app;
}

if ($("#inventorylist").get(0) === undefined) {
    window.spinner = {
        showSpinner: () => {
            document.querySelector('#spinner').style.display = 'block';
        },

        hideSpinner: (error) => {
            document.querySelector('#spinner').style.display = 'none';
            if (error) {
                alert(message);
                throw new Error(message);
            }
        },
    };

    window.spinner.showSpinner();
    $.get("https://bad-dragon.com/inventorytoys/showlist/all").done((data) => {
        // make images lazy to prevent load
        data = data.replace(/img src=/g, 'img class="lazy-img" lazy-src=');

        // parse into an array of elements
        var parser = new DOMParser();
        window.bdDOM = parser.parseFromString(data, 'text/html');

        // find the sitearea TODO maybe do better
        window.bdTable = bdDOM.getElementById('sitearea');

        // add to the document
        document.body.appendChild(bdTable);

        window.app = bootstrap(false);
    });
}
else {
    // TODO add spinner
    window.spinner = {
        showSpinner: () => {
            document.body.style.cursor = 'wait';
        },

        hideSpinner: (error) => {
            document.body.style.cursor;
            if (error) {
                alert(message);
                throw new Error(message);
            }
        },
    };

    // try to lessen the load from bd assets
    // $('img').each(function(i, img) {
    //     img.setAttribute('lazy-src', img.src);
    //     img.removeAttribute('src');
    //     img.classList.add('lazy-img');
    // });

    window.app = bootstrap(true);
}
